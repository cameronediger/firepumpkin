﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;

namespace FirePumpkin.Helpers
{
    public class ParticleAPI
    {
		protected string _BaseUrl = "";
		protected string _ApiPath = "";
		protected string _AccessToken = "";

		protected HttpClient _httpClient;

		public ParticleAPI(ParticleAPISettings settings)
		{
			this._httpClient = new HttpClient();
			if(settings != null)
			{
				this._BaseUrl = settings.BaseURL;
				this._ApiPath = settings.APIPath;
				this._AccessToken = settings.AccessToken;
			}
		}

		public CurrentStatusResponse Test(string strDeviceId)
		{
			CurrentStatusResponse resp = null;

			if (_httpClient != null)
			{
				this._httpClient.BaseAddress = new Uri(this._BaseUrl);
				this._httpClient.DefaultRequestHeaders.Add("dataType", "json");

				string strRequestAddress = string.Format("{0}/{1}/currstatus?access_token={2}", this._ApiPath, strDeviceId, this._AccessToken);
				HttpResponseMessage response = _httpClient.GetAsync(strRequestAddress).Result;
				if(response.IsSuccessStatusCode)
				{
					resp = JsonConvert.DeserializeObject<CurrentStatusResponse>(response.Content.ReadAsStringAsync().Result);
				}
			}
			
			return resp;
		}

		public ParticleFunctionResponse SendFire(string deviceId)
		{
			ParticleFunctionResponse ret = null;

			if(_httpClient != null)
			{
				_httpClient.BaseAddress = new Uri(this._BaseUrl);
				_httpClient.DefaultRequestHeaders.Accept.Clear();

				ParticleArgs args = new ParticleArgs();
				args.command = "on";
				string strRequestAddress = string.Format("{0}/{1}/fire?access_token={2}", this._ApiPath, deviceId, this._AccessToken);
				string argsJsonString = JsonConvert.SerializeObject(args);
				HttpResponseMessage response = _httpClient.PostAsync(strRequestAddress, new StringContent(args.ToString(), System.Text.Encoding.UTF8)).Result;
				if (response.IsSuccessStatusCode)
					ret = JsonConvert.DeserializeObject<ParticleFunctionResponse>(response.Content.ReadAsStringAsync().Result);
			}

			return ret;
		}
	}

	public class ParticleAPISettings
	{
		public string BaseURL { get; set; }
		public string APIPath { get; set; }
		public string AccessToken { get; set; }
	}

	public class CurrentStatusResponse
	{
		public string cmd { get; set; }
		public string name { get; set; }
		public int result { get; set; }
		public CoreInfo coreInfo { get; set; }
	}

	public class CoreInfo
	{
		public string last_app { get; set; }
		public DateTime last_heard { get; set; }
		public bool connected { get; set; }
		public string deviceID { get; set; }
	}

	public class ParticleFunctionResponse
	{
		public string id { get; set; }
		public string last_app { get; set; }
		public bool connected { get; set; }
		public int return_value { get; set; }
	}

	public class ParticleArgs
	{
		public string command { get; set; }

		public override string ToString()
		{
			return "args=" + this.command;
		}
	}
}

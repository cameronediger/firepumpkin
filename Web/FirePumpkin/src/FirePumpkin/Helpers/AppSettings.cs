﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FirePumpkin.Helpers
{
    public class AppSettings
    {
		public int LastAlertWaitSeconds { get; set; }
		public ParticleAPISettings ParticleAPI { get; set; }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using FirePumpkin.Helpers;

namespace FirePumpkin.Controllers
{
    public class HomeController : Controller
    {
		private readonly AppSettings _appSettings;
		private ParticleAPI _particleApi;

		public HomeController(IOptions<AppSettings> appSettings)
		{
			this._appSettings = appSettings.Value;
			if (this._appSettings != null)
				this._particleApi = new ParticleAPI(_appSettings.ParticleAPI);
		}

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

		[HttpGet]
		public JsonResult SendFire()
		{
			if (this._particleApi == null)
			{
				if (this._appSettings != null)
					this._particleApi = new ParticleAPI(_appSettings.ParticleAPI);
				else
					return Json("NULLS");
			}

			if (this._particleApi != null)
			{
				ParticleFunctionResponse resp = this._particleApi.SendFire("1b0038000447343138333038");
				if(resp != null)
					return Json(string.Format("resp: {0}", resp.return_value));
			}
				
			return Json("NULL");
		}
    }
}

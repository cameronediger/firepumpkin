int nLedPin = D7;
int nControlPin = D3;


void setup() {
    pinMode(nControlPin, OUTPUT);
    pinMode(nLedPin, OUTPUT);
    
    Particle.function("fire", CreateFire);
    
    digitalWrite(nControlPin, LOW);
    digitalWrite(nLedPin, LOW);
}

void loop() {
    // nothing to do here
}

int CreateFire(String command)
{
    digitalWrite(nControlPin, HIGH);
    digitalWrite(nLedPin, HIGH);
    delay(750);
    digitalWrite(nControlPin, LOW);
    digitalWrite(nLedPin, LOW);
    
    return 0;
}